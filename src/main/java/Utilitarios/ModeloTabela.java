/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilitarios;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author usuario
 */
public class ModeloTabela extends AbstractTableModel {

    private ArrayList linhas = null;
    private String[] colunas = null; // vetor do tipo string

    public ModeloTabela(ArrayList lin, String[] col) {
        setLinhas(lin);
        setColunas(col);
    }

    public int getRowCount() {
        return linhas.size();
    }

    public ArrayList getLinhas() {
        return linhas;
    }

    public void setLinhas(ArrayList linhas) {
        this.linhas = linhas;
    }

    public String[] getColunas() {
        return colunas;
    }

    public void setColunas(String[] colunas) {
        this.colunas = colunas;
    }

    public int getColumnCount() {
        return colunas.length;
    }

    public String getColumnName(int numCol) {//Pega o valor do nome da coluna
        return colunas[numCol];
    }

    @Override
    public Object getValueAt(int numLin, int numCol) { // Metodo responsavel por montar a tabela
        Object[] linha = (Object[]) getLinhas().get(numLin);
        return linha[numCol];
    }

}
