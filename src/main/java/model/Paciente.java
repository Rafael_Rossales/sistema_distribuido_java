/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Utilitarios.Pessoa;

/**
 *
 * @author usuario
 */
public class Paciente extends Pessoa {

    private String cpf;
    private String CartaoConvenio;
    private String pesquisar;
    private String sexo;

    public Paciente(int id, String nome, String sobrenome, String endereco, String dataNascimento, String telefone, String cpf, String CartaoCovenio,String sexo, String pesquisar,String email) {
        super(id, nome, sobrenome, endereco, dataNascimento, telefone,email);
        this.cpf = cpf;
        this.CartaoConvenio = CartaoConvenio;
        this.sexo = sexo;
        this.pesquisar = pesquisar;
    }

    public Paciente() {
        super(0, null, null, null, null, null, null);
    }

    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCartaoConvenio() {
        return CartaoConvenio;
    }

    public void setCartaoConvenio(String CartaoConvenio) {
        this.CartaoConvenio = CartaoConvenio;
    }

    public String getPesquisar() {
        return pesquisar;
    }

    public void setPesquisar(String pesquisar) {
        this.pesquisar = pesquisar;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

}
