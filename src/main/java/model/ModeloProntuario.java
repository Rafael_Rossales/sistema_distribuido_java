/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Utilitarios.Conexao;
import Utilitarios.Metodos;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class ModeloProntuario implements Metodos {

    Conexao conec = new Conexao();
    Prontuario pronturario = new Prontuario();

    @Override
    public String salvar(Object ob) {
        Prontuario pront = (Prontuario) ob; // conversao de Objeto
        conec.Conexao();
        try {
            PreparedStatement pst = conec.conn.prepareStatement("INSERT INTO prontuario (Diagnostico,Consulta_id) VALUES (?,?)");
            pst.setString(1, pront.getDiagnostico());
            pst.setInt(2, pront.getConsultaId());

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Consulta inserido com sucesso");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Consulta nao  inserido " + e);
        }

        conec.desconectar();

        return null;

    }

    @Override
    public String editar(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String excluir(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
