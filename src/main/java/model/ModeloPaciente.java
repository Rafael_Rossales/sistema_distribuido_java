/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Utilitarios.Metodos;
import Utilitarios.Conexao;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class ModeloPaciente implements Metodos {

    Conexao conec = new Conexao();
    Paciente paciente = new Paciente();

    @Override
    public String salvar(Object ob) {
        Paciente pac = (Paciente) ob;
        conec.Conexao();
        try {
            PreparedStatement pst = conec.conn.prepareStatement("INSERT INTO usuario(Nome,Sobrenome,Endereco,DataNascimento,Sexo,Cpf,Telefone,CartaoConvenio) VALUES(?,?,?,?,?,?,?,?)");
            pst.setString(1, pac.getNome());
            pst.setString(2, pac.getSobrenome());
            pst.setString(3, pac.getEndereco());
            pst.setString(4, pac.getDataNascimento());
            pst.setString(5, pac.getSexo());
            pst.setString(6, pac.getCpf());
            pst.setString(7, pac.getTelefone());
            pst.setString(8, pac.getCartaoConvenio());

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Paciente Inserido com Sucesso!");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Inserir Paciente!" + ex);
        }

        conec.desconectar();

        return null;

    }

    @Override
    public String editar(Object ob) {
        Paciente pac = (Paciente) ob;
        conec.Conexao();

        try {
            PreparedStatement pst = conec.conn.prepareStatement("update usuario set Nome=?,Sobrenome=?,Endereco=?,dataNacimento=?,Sexo=?,CPF=?,Telefone=?,CartaoConvenio=? Where id=? ");

            pst.setString(1, pac.getNome());
            pst.setString(2, pac.getSobrenome());
            pst.setString(3, pac.getEndereco());
            pst.setString(4, pac.getDataNascimento());
            pst.setString(5, pac.getSexo());
            pst.setString(6, pac.getCpf());
            pst.setString(7, pac.getTelefone());
            pst.setString(8, pac.getCartaoConvenio());
            pst.setInt(9, pac.getId());

            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados Alterados com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar dados!" + e);
        }
        return null;

    }

    @Override
    public String excluir(Object ob) {
        Paciente pac = (Paciente) ob;
        conec.Conexao();
        try {
            PreparedStatement pst = conec.conn.prepareStatement("delete from usuario where idusuario = ?");
            pst.setInt(1, pac.getId());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Paciente Excluido com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir Pacienete!" + e);
        }
        conec.desconectar();

        return null;

    }

}
