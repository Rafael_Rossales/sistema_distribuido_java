/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Rafael_Rossales
 */
public class Agenda {
    
    int idconsulta;
    String data;
    String hora;
    int idMedico;
    int idUsuario;

    public Agenda() {
    }
   

    public Agenda(int idconsulta, String data, String hora, int idMedico, int idUsuario) {
        this.idconsulta = idconsulta;
        this.data = data;
        this.hora = hora;
        this.idMedico = idMedico;
        this.idUsuario = idUsuario;
    }

    public int getIdconsulta() {
        return idconsulta;
    }

    public void setIdconsulta(int idconsulta) {
        this.idconsulta = idconsulta;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(int idMedico) {
        this.idMedico = idMedico;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    
    
    
    

    
    
    
}
