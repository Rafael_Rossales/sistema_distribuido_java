/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Utilitarios.Conexao;
import Utilitarios.Metodos;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ModeloConsulta implements Metodos {

    Conexao conec = new Conexao();
    Consulta consulta = new Consulta();
    Medico medico = new Medico();
    Paciente paciente = new Paciente();

    @Override
    public String salvar(Object ob) {
        Consulta con = (Consulta) ob;

        conec.Conexao();
        try {
            PreparedStatement pst = conec.conn.prepareStatement("INSERT INTO consulta(Data,Hora,Paciente_id,Medico_id)VALUES(?,?,?,?)");
            pst.setString(1, con.getData());
            pst.setString(2, con.getHora());
            pst.setInt(3, con.getIdPaciente());
            pst.setInt(4, con.getIdMedico());

            pst.execute();

            JOptionPane.showMessageDialog(null, "Consulta Agendada com sucesso!");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Agendar Consulta" + ex);
        }

        conec.desconectar();
        return null;
    }

    @Override
    public String editar(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String excluir(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
