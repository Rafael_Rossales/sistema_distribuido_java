/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
 
import Utilitarios.Metodos;
import Utilitarios.Conexao;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import visao.TelaConsultas;
 
/**
 *
 * @author usuario
 */
public class ModeloMedico implements Metodos {
 
    Conexao conec = new Conexao();
    Medico medico = new Medico();
 
    @Override
    public String salvar(Object ob) {
        Medico med = (Medico) ob; // conversao de Objeto
        conec.Conexao();
        try {
            PreparedStatement pst = conec.conn.prepareStatement("INSERT INTO medico(nome,sobrenome,crm,endereco,dataNascimento,telefone,especializacao) values(?,?,?,?,?,?,?)");
            pst.setString(1, med.getNome());
            pst.setString(2, med.getSobrenome());
            pst.setString(3, med.getCrm());
            pst.setString(4, med.getEndereco());
            pst.setString(5, med.getDataNascimento());
            pst.setString(6, med.getTelefone());
            pst.setString(7, med.getEspecializacao());

 
            pst.executeUpdate();
 
            JOptionPane.showMessageDialog(null, "Medico (a) Cadastrado com sucesso!");
 
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir medico" + e);
        }
        conec.desconectar();
        return null;
    }
 
    public Object buscar(Object ob) {
        Medico med = (Medico) ob;
        conec.Conexao();
 
        try {
            conec.executaSql("SELECT * FROM medicos WHERE nomeMedico like '%" + med.getPesquisa() + "%'");
            conec.rs.first(); // Pega o primeiro resultado encontrado no banco
             
            med.setNome(conec.rs.getString("nomeMedico"));
            med.setSobrenome(conec.rs.getString("sobrenome"));
            med.setEndereco(conec.rs.getString("endereco"));
            med.setDataNascimento(conec.rs.getString("dataNascimento"));
            med.setTelefone(conec.rs.getString("telefone"));
            med.setCrm(conec.rs.getString("crm"));
            med.setEspecializacao(conec.rs.getString("especializacao"));
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Medico nao encontrado!" + e);
        }
        conec.desconectar();
        return null;
 
    }
 
    @Override
    public String excluir(Object ob) {
        Medico med = (Medico) ob;
        conec.Conexao();
        try {
            PreparedStatement pst = conec.conn.prepareStatement("delete from medicos where idmedico =?");
            pst.setInt(1, med.getId());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Excluido com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir medico!" + e);
        }
        conec.desconectar();
        return null;
 
    }
 
    @Override
    public String editar(Object ob) {
        Medico med = (Medico) ob;
        conec.Conexao();
 
        try {
            PreparedStatement pst = conec.conn.prepareStatement("update medicos set nomeMedico =?,sobrenome=?,endereco=?,dataNascimento=?,telefone=?,crm=?,especializacao=? where idmedico =?");
 
            pst.setString(1, med.getNome());
            pst.setString(2, med.getSobrenome());
            pst.setString(3, med.getEndereco());
            pst.setString(4, med.getDataNascimento());
            pst.setString(5, med.getTelefone());
            pst.setString(6, med.getCrm());
            pst.setString(7, med.getEspecializacao());
            pst.setInt(8, med.getId());
 
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados alterados com sucesso!");
 
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar dados!" + ex);
        }
        return null;
    }
    private javax.swing.JComboBox box;
 
    public void ListarMedicos() {
         
    }
}