/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Utilitarios.Pessoa;

/**
 *
 * @author usuario
 */
public class Medico extends Pessoa {

    private String crm;
    private String especializacao;
    private String pesquisa;

    public Medico() {
        super(0, null, null, null, null, null,null);
    }

    public Medico(String crm, String especializacao, String pesquisa, int id, String nome, String sobrenome, String endereco, String dataNascimento, String telefone,String email) {
        super(id, nome, sobrenome, endereco, dataNascimento, telefone,email);
        this.crm = crm;
        this.especializacao = especializacao;
        this.pesquisa = pesquisa;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getEspecializacao() {
        return especializacao;
    }

    public void setEspecializacao(String especializacao) {
        this.especializacao = especializacao;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    @Override
    public String toString() {
        return super.getNome();
    }

}
