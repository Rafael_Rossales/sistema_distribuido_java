<h1>Sistema Distribuído</h1>

<h2>Introdução</h2>
  <p>O sistema de administração clinico, desenvolvido em java tem como finalidade auxiliar admnistradores de clinicas, o sistema desktop
  possui diversar funcionalidade tais como: cadastro,edição e exclusão de médicos e usuários, também possuindo funcionalidades de gerenciamento e agendamentos.</p>

<h3>Linguagens</h3> 
<ul>
<li>JAVA</li>
<li>MYSQL</li>
</ul>


<h3>Instruções</h3>
  <p>As instruções a seguir fornecerão uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste,
  é necessário a instalação de um servidor Xampp ou semelhantes, também é necessário incluir bibliotecas mysql para que o sistema realize
  a conexão com o banco de dados.</p>

<h2>Clonar</h2>
<p>Para efetuar a clonagem do projeto, utilizando os <i>links</i> disponíveis no repositório GitLab
é nescessário possuir o <i>Software</i> Git instalado na sua máquina. Para mais informações de instalação
acesse o site do [Git](https://git-scm.com/) .Caso você ja possua o Git em sua máquina você pode
baixar este projeto através dos <i>Links</i> disponíveis abaixo.</p>

<h2>Clone com SSH</h2>
 <ul>
 <li>git clone git@gitlab.com:Rafael_Rossales/sistema_distribuido_java.git</li>
 </ul>
 
 <h2>Clone com HTTPS</h2>
 <ul>
 <li>git clone https://gitlab.com/Rafael_Rossales/sistema_distribuido_java.git</li>
 </ul>
 
 <h2>Liscença</h2>
 <p>Este projeto conta com a seguinte licença de uso: Instituto de Tecnologia de <i>Massachusetts</i> [MIT](https://pt.wikipedia.org/wiki/Licen%C3%A7a_MIT).</p>
